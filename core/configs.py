from pymongo import MongoClient

client = MongoClient("mongodb://localhost:27017")
database = client['devdb']
story_collection = database['stories']

try:
    print(client.server_info())
except Exception:
    print("Unable to connect to the server.")
