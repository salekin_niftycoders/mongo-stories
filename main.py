import json
from typing import Any, List
from bson.json_util import dumps
from fastapi import FastAPI, Body
from fastapi.encoders import jsonable_encoder
from starlette import status
from core.configs import story_collection
from schemas.stories import Story
from bson import ObjectId

app = FastAPI()


@app.get("/", summary='Get all stories')
def read_story() -> Any:
    stories = story_collection.find()
    return to_json(stories)


@app.post(
    "/", response_description="Add new story",
    response_model=Any, status_code=status.HTTP_201_CREATED
)
def create_story(story: Story = Body(...)):
    story = jsonable_encoder(story)
    story = story_collection.insert_one(story)
    return to_json(story.inserted_id)


@app.get("/{id}", response_description="Story retrieved")
def get_story(id):
    story = story_collection.find_one({"_id": ObjectId(id)})
    return to_json(story)


@app.put("/{id}", status_code=status.HTTP_200_OK)
def update_story(id, story: Story):
    story = story_collection.find_one_and_update({"_id": ObjectId(id)}, {"$set": dict(story)}, return_document=True)
    return to_json(story)


@app.delete("/{id}", status_code=status.HTTP_200_OK)
def delete_story(id):
    story_collection.find_one_and_delete({"_id": ObjectId(id)})


def to_json(item):
    return json.loads(dumps(item))
