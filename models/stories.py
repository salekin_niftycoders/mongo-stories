from pydantic.main import BaseModel


class Story(BaseModel):
    title: str
    description: str
